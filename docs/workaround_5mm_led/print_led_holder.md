# LED Holder: additional print

If you don't have the illumination PCB, you will need to print an extra part to hold a 5mm LED instead.

{{BOM}}

[PLA filament]: ../parts/materials/pla_filament.md "{cat:material}"
[RepRap-style printer]: ../parts/tools/rep-rap.md "{cat:tool}"

## Print the condenser LED holder {pagestep}

![condenser LED holder](../models/condenser_led_holder.stl)

Using a [RepRap-style printer]{qty:1} and some [PLA filament]{qty: 5 grams}, print [condenser LED holder.stl](../models/condenser_led_holder.stl){previewpage}.

[condenser LED holder]{output, qty:1, hidden}

