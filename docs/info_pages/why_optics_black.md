# Why print the optics module or lens spacer in black?

The optics module (high resolution microscope) or lens spacer (low-cost microscope) is used to separate the imaging optics from the camera sensor. In an ideal microscope the only light that reaches the camera sensor should be the light focused by these optics. To improve the performance of the OpenFlexure microscope the optics module has a built-in ridges light trap to try to catch stray light:

![cutaway](../renders/optics_assembled.png)

The purpose of these ridges is to make the number of reflections off the optics module as high as possible, compared to if the internal walls were flat.

![cutaway](../diagrams/light-trap.png)

The other way we can limit reflected light is to use black filament. This as this will absorb more of the stray light.

Black filament is also more opaque than other filaments. This means that less light will be transmitted through the walls of the optics module.